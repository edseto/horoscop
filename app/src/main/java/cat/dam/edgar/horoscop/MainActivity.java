package cat.dam.edgar.horoscop;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private TextView tv_selectDate, tv_age, tv_chineseName, tv_zodiacName, tv_selectHour, tv_hour, tv_ascendentName;
    private HashMap<Integer, String> chinese = new HashMap<>();
    private ImageView ig_chineseAnimal, ig_zodiacSign, ig_ascendentSign;
    private LinearLayout ll_horoscop, ll_ascendent;
    private String zodiacSign;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setVariables();
        birthDate();
        birthHour();
    }

    private void setVariables()
    {
        setMainVariables();
        setZodiacVariables();
        setChineseVariables();
        setAscendentVariables();
    }

    private void setMainVariables()
    {
        tv_selectDate = (TextView) findViewById(R.id.tv_selectDate);
        tv_age = (TextView) findViewById(R.id.tv_age);
        tv_selectHour = (TextView) findViewById(R.id.tv_selectHour);
        tv_hour = (TextView) findViewById(R.id.tv_hour);
        ll_horoscop = (LinearLayout) findViewById(R.id.ll_horoscop);
    }

    private void setZodiacVariables()
    {
        tv_zodiacName = (TextView) findViewById(R.id.tv_zodiacName);
        ig_zodiacSign = (ImageView) findViewById(R.id.ig_zodiacSign);
    }

    private void setChineseVariables()
    {
        tv_chineseName = (TextView) findViewById(R.id.tv_chineseName);
        ig_chineseAnimal = (ImageView) findViewById(R.id.ig_chineseAnimal);
        setChineseHoroscop();
    }

    private void setChineseHoroscop()
    {
        chinese.put(1959, "Porc");
        chinese.put(1960, "Rata");
        chinese.put(1961, "Bou");
        chinese.put(1962, "Tigre");
        chinese.put(1963, "Conill");
        chinese.put(1964, "Drac");
        chinese.put(1965, "Serp");
        chinese.put(1966, "Cavall");
        chinese.put(1967, "Cabra");
        chinese.put(1968, "Mico");
        chinese.put(1969, "Gall");
        chinese.put(1970, "Gos");
    }

    private void setAscendentVariables()
    {
        ll_ascendent = (LinearLayout) findViewById(R.id.ll_ascendent);
        tv_ascendentName = (TextView) findViewById(R.id.tv_ascendentName);
        ig_ascendentSign = (ImageView) findViewById(R.id.ig_ascendentSign);
    }

    private void birthDate()
    {
        tv_selectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                int year, month, day;
                year = calendar.get(Calendar.YEAR);
                month = (calendar.get(Calendar.MONTH));
                day = calendar.get(Calendar.DAY_OF_MONTH);

                showDatePickerDialog(day, month, year);
            }
        });
    }

    private void showDatePickerDialog(int actualDay, int actualMonth, int actualYear)
    {
        DatePickerDialog dateSelector = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                month++;
                showAge(actualDay, actualMonth, actualYear, day, month, year);
                showChineseHoroscop(year);
                calculateZodiacHoroscop(day, month);
                ll_horoscop.setVisibility(View.VISIBLE);
            }
        }, actualYear, actualMonth, actualDay);

        dateSelector.show();
    }

    private void showAge(int actualDay, int actualMonth, int actualYear, int day, int month, int year)
    {
        int age = calculateAge(actualDay, actualMonth, actualYear, day, month, year);
        String have, years;
        have = getResources().getString(R.string.have);
        years = getResources().getString(R.string.years);

        tv_age.setText(have + " " +age + " " + years);
    }

    private int calculateAge(int actualDay, int actualMonth, int actualYear, int day, int month, int year)
    {
        if((month<=actualMonth)&&(day<=actualDay)) return actualYear - year;
        else return (actualYear - year) - 1;
    }

    private void showChineseHoroscop(int year)
    {
        boolean find = false;
        int rest, chineseYear;
        String animal ="";
        Iterator iterator = chinese.entrySet().iterator();
        Map.Entry entry;

        while (iterator.hasNext() && !find){
            entry = (Map.Entry) iterator.next();
            chineseYear = Integer.parseInt(entry.getKey().toString());
            rest = (chineseYear-year)%12;
            if(rest == 0){
                find = true;
                animal = entry.getValue().toString().toLowerCase();
            }
        }
        showChineseImage(animal);
    }

    private void showChineseImage(String animal)
    {
        int idImage;
        tv_chineseName.setText(getResources().getIdentifier(animal, "string", getPackageName()));

        idImage = getResources().getIdentifier(animal, "drawable", getPackageName());
        ig_chineseAnimal.setImageDrawable(getDrawable(idImage));
        ig_chineseAnimal.setContentDescription(animal);
    }

    private void calculateZodiacHoroscop(int day, int month)
    {
        String acuario="acuario", capricornio="capricornio", piscis="piscis", aries="aries";
        String tauro="tauro", geminis="geminis", cancer="cancer", leo="leo", virgo="virgo";
        String libra="libra", escorpio="escorpio", sagitario="sagitario";
        switch (month){
            case 1:
                if(day>20) showZodiacImage(acuario);
                else showZodiacImage(capricornio);
                break;
            case 2:
                if(day>19) showZodiacImage(piscis);
                else showZodiacImage(acuario);
                break;
            case 3:
                if(day>20) showZodiacImage(aries);
                else showZodiacImage(piscis);
                break;
            case 4:
                if(day>20) showZodiacImage(tauro);
                else showZodiacImage(aries);
                break;
            case 5:
                if(day>20) showZodiacImage(geminis);
                else showZodiacImage(tauro);
                break;
            case 6:
                if(day>21) showZodiacImage(cancer);
                else showZodiacImage(geminis);
                break;
            case 7:
                if(day>22) showZodiacImage(leo);
                else showZodiacImage(cancer);
                break;
            case 8:
                if(day>23) showZodiacImage(virgo);
                else showZodiacImage(leo);
                break;
            case 9:
                if(day>22) showZodiacImage(libra);
                else showZodiacImage(virgo);
                break;
            case 10:
                if(day>22) showZodiacImage(escorpio);
                else showZodiacImage(libra);
                break;
            case 11:
                if(day>22) showZodiacImage(sagitario);
                else showZodiacImage(escorpio);
                break;
            default:
                if(day>21) showZodiacImage(capricornio);
                else showZodiacImage(sagitario);
                break;
        }
    }

    private void showZodiacImage(String sign)
    {
        int idImage;
        zodiacSign = sign;
        tv_zodiacName.setText(getResources().getIdentifier(sign, "string", getPackageName()));

        idImage = getResources().getIdentifier(sign, "drawable", getPackageName());
        ig_zodiacSign.setImageDrawable(getDrawable(idImage));
        ig_zodiacSign.setContentDescription(sign);
    }

    private void birthHour()
    {
        tv_selectHour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                int hour, minute;
                hour = calendar.get(Calendar.HOUR_OF_DAY);
                minute = calendar.get(Calendar.MINUTE);

                showTimePicker(hour, minute);
            }
        });
    }

    private void showTimePicker(int actualHour, int actualMinute)
    {
        TimePickerDialog selectTime;
        selectTime = new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hour, int minute) {
                String textHour = getResources().getString(R.string.birth_hour);
                tv_hour.setText(textHour + " " +String.format("%02d:%02d", hour, minute));
                calculateAscendentSign(hour);
                ll_ascendent.setVisibility(View.VISIBLE);
            }
        }, actualHour, actualMinute, true);

        selectTime.setTitle(getResources().getString(R.string.select_hour));
        selectTime.show();
    }

    private void calculateAscendentSign(int hour)
    {
        ArrayList<String> ascendentSigns = new ArrayList<>();
        createArrayList(ascendentSigns);

        String[] signs = {"aries", "tauro", "geminis", "cancer", "leo", "virgo", "libra", "escorpio", "sagitario", "capricornio", "acuario", "piscis"};
        int index = Arrays.asList(signs).indexOf(zodiacSign);

        restructureArray(ascendentSigns, index);
        index = getAscendentIndex(hour);
        showAscendentImage(ascendentSigns.get(index));
    }

    private void createArrayList(ArrayList<String> ascendentSigns)
    {
        String[] signs = {"capricornio", "acuario", "piscis", "aries", "tauro", "geminis", "cancer", "leo", "virgo", "libra", "escorpio", "sagitario"};

        for (String sign: signs) {
            ascendentSigns.add(sign);
        }
    }

    private void restructureArray(ArrayList<String> ascendentSigns, int index)
    {
        String temp;
        for (int i = 0; i < index; i++) {
            temp = ascendentSigns.get(0);
            ascendentSigns.remove(0);
            ascendentSigns.add(temp);
        }
    }

    private int getAscendentIndex(int hour)
    {
        if(hour<2) return 0;
        if(hour<4) return 1;
        if(hour<6) return 2;
        if(hour<8) return 3;
        if(hour<10) return 4;
        if(hour<12) return 5;
        if(hour<14) return 6;
        if(hour<16) return 7;
        if(hour<18) return 8;
        if(hour<20) return 9;
        if(hour<22) return 10;

        return 11;
    }

    private void showAscendentImage(String sign)
    {
        int idImage = getResources().getIdentifier(sign, "drawable", getPackageName());
        ig_ascendentSign.setImageDrawable(getDrawable(idImage));
        ig_ascendentSign.setContentDescription(sign);

        tv_ascendentName.setText(getResources().getIdentifier(sign, "string", getPackageName()));
    }
}